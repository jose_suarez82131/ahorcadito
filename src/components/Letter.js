import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Letter extends Component {

    state = {
        sendLetter: ''
    }

    sendLetra = (letra) => {
        this.setState({sendLetter: letra})
    }

    static propTypes = {
        letra: PropTypes.string,
        active: PropTypes.string
    }
    render() {
        const { letra} = this.props
        return (
            <div>
                <button onClick={this.sendLetra}>{letra}</button>
            </div>
        )
    }
}
