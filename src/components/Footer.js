import React from 'react'

export default function Footer() {
    return (
        <div>
            <footer>
                <b/>
                Presentado por José Andrés Suárez Espinal
                C.C. 71387452
            </footer>
        </div>
    )
}
