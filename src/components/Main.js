import React, { Component } from 'react'
import Title from './Title'
import Picture from './Picture'
import Footer from './Footer'
import Letters from './Letters'

const ABC = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O',
'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z']

const WORDS = [
    'EGIPTO',
    'ARISTOTELES',
    'PROGRAMACION',
    'PROFE MISERICORDIA',
    'NAPOLEON'
]

export default class Main extends Component {

    currentLevel = 5
    currentWord = WORDS[Math.floor(Math.random() * (5))]
    render() {
        
        return (
            <div>
                <Title>Ahorcadito - Parcial</Title>
                <Picture 
                    intentos = {this.currentLevel} 
                    imagen = {this.currentLevel + 'ToEnd.jpg'}
                    palabra = {this.currentWord}
                />
                <Letters letras = {ABC}/>
                <Footer></Footer>
            </div>
        )
    }
}
