import React, { Component } from 'react'
import PropTypes from 'prop-types'

export default class Picture extends Component {

    static propTypes = {
        intentos: PropTypes.number,
        imagen: PropTypes.string
    }
    
    render() {
        const {intentos, imagen, palabra} = this.props
        return (
            <div>
                <p>Intentos restantes: {intentos}</p>
                <img src={imagen}/>
                <p>{palabra}</p>
            </div>
        )
    }
}
