import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Letter from './Letter'

export default class Letters extends Component {

    state = {
        sendLetter: ''
    }
    static propTypes = {
        letras: PropTypes.array
    }

    sendingLetter = (letra) => {
        this.setState({sendLetter: letra});
        console.log(this.state.sendLetter)
    }
    render() {
        const {letras} = this.props
        return (
            <div>
                    {letras.map((letra, index) => {
                        return(
                            <div key={index}>
                            <Letter
                                OnClick = {this.sendingLetter}
                                letra = {letra}
                            />
                            </div>
                        )
                    })}
            </div>
        )
    }
}
